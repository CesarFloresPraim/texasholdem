﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TexasHoldem.Clases;
using TexasHoldem.Enums;

namespace TexasHoldem
{
    public partial class Form1 : Form
    {
        Baraja baraja = new Baraja();
        List<Carta> barajaOrdenada = new List<Carta>();
        List<Carta> barajaRevuelta = new List<Carta>();
        Reglas reglas = new Reglas();
        Dealer dealer = new Dealer();
        Jugadores jugadores = new Jugadores();
        
        public Form1(int numeroJugadores)
        {
            InitializeComponent();
            barajaOrdenada = baraja.CrearBaraja();
            reglas.NumeroJugadores = numeroJugadores;

            for(int i = 1; i <= reglas.NumeroJugadores; i++)
            {
                
                Jugador jugador = new Jugador(i,1500,null);
                jugadores.AgregarJugador(jugador);
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void btn_check_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            foreach (Carta carta in barajaOrdenada)
            {
                listBox1.Items.Add("Numero:" + carta.Numero.ToString() + " - Color: " + carta.Propiedades.Color.ToString() + " - Palo: " + carta.Propiedades.Palo.ToString());
            }
        }

        private void btn_subir_apuesta_Click(object sender, EventArgs e)
        {
            
            listBox1.Items.Clear();
            barajaRevuelta = baraja.Revolver(barajaOrdenada);

            foreach (Carta cartaa in barajaRevuelta)
            {
                listBox1.Items.Add("Numero:" + cartaa.Numero.ToString() + " - Color: " + cartaa.Propiedades.Color.ToString() + " - Palo: " + cartaa.Propiedades.Palo.ToString());
            }
 
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btn_repartir_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < 2; i++)//Ciclo de 2 para repartir 2 cartas iniciales
            {
                foreach (Jugador jugador in jugadores.ListaJugadores)// A cada jugador repartir una carta
                {
                    dealer.Repartir(barajaRevuelta, reglas.JugadorEnTurno, jugadores);
                    reglas.siguienteJugador();
                }
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            ////
        }
    }
}
